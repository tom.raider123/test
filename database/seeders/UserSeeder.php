<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $user = User::create([
            'first_name' => 'admin', 
            'last_name' => 'admin', 
            'email' => 'admin@admin.com',
            'profile_image' => 'https://via.placeholder.com/150',
            'department_id' => '1,2',
            'password' => bcrypt('password'),    
        ]);
    }
}
