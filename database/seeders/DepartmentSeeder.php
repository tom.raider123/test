<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();
		$department = array(
			array('name' => "Sales",'description' =>"Sales"),
			array('name' => "Marketing",'description' =>"Marketing"),

        );
        Department::insert($department);
    }
}
