<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use Validator;
use Auth;
use DataTables;
use Config;
use Form;
use DB;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/departments/index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
      
        $validator = Validator::make($request->all(), [
            'name'       => 'required', 
            'description'  => 'required', 
        ]);     

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


      
            $data = $request->all();
            $user = Department::create($data);
            $request->session()->flash('success','Department added Successfully');
            return redirect()->to('admin/departments');
         

        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $department = Department::findOrFail($id);
            return view('admin.departments.create',compact('department'));
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $validator = Validator::make($request->all(), [
                'name'       => 'required|unique:departments,name,'.$id, 
                'description'        => 'required', 
            ]);     
    
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
    
    
                $department = Department::findOrFail($id);
                $data = $request->all();
                $department->update($data);
                $request->session()->flash('success','department updated Successfully');
                return redirect()->to('admin/departments');
    
            }catch (\Exception $e){
                return redirect()->back()->with('error',$e->getMessage());
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{ 
            $department = Department::findOrFail($id);
            $department->delete();
            session()->flash('danger','Department deleted successfully');
            return redirect()->to('admin/departments');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function getDepartments(Request $request){
        $department = Department::select([\DB::raw(with(new Department)->getTable().'.*')]);
      return DataTables::of($department)
          ->editColumn('created_at', function($department){
              return date('Y-m-d', strtotime($department->created_at));
          })            
          ->editColumn('name', function($department){
              return $department->name;
          })
          ->editColumn('description', function($department){
            return $department->description;
        })
         
          ->addColumn('action', function ($department) {
              return
                  // edit
                  '<a href="'.route('departments.edit',[$department->id]).'" class="btn btn-success btn-circle btn-sm"><i class="fas fa-edit"></i></a> '.
                  // Delete
                    Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit'=>"return confirm('Do you really want to delete?')",
                                'route' => ['departments.destroy', $department->id])).
                    ' <button type="submit" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>'.
                    Form::close();
          })
          ->rawColumns(['action'])
          ->make(true);
  }
}
