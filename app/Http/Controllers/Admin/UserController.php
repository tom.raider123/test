<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Validator;
use Auth;
use DataTables;
use Config;
use Form;
use DB;
use Hash;
use App\Mail\NewUserMail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/users/index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::select('id','name')->get();
        return view('admin.users.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // dd($request->all());


       
        $validator = Validator::make($request->all(), [
            'first_name'       => 'required', 
            'last_name'        => 'required', 
            'email'         => 'required|email|unique:users,email', 
            'department_id'    => 'required',           
            'profile_image'    => 'image|max:2048',
            'password'          => 'required',
            'password_confirmation' => 'required|same:password'
        ]);     

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


      
            $data = $request->all();
           
            $data['password'] = Hash::make($request->password);
            $data['department_id'] = implode(',',$data['department_id']);
            $profile_image =  $request->file('profile_image');
              if($profile_image){
                $imageName = time().'.'.$profile_image->extension();  
                $profile_image->storeAs('images', $imageName);
                $data['profile_image'] = $imageName;
              }else{
                $data['profile_image'] = 'https://via.placeholder.com/150';
                
              }
            $user = User::create($data);
            $request->session()->flash('success','User added Successfully');
            // send mail
            $details = [
                'title' => 'Welcome Mail from Matellio',
                'body' => 'User has been created'
            ];
            \Mail::to($data['email'])->send(new NewUserMail($details));
            //
            return redirect()->to('admin/users');
         

        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try{
            $user = User::findOrFail($id);
            $department = Department::select('id','name')->get();
            return view('admin.users.create',compact('user','department'));
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $validator = Validator::make($request->all(), [
            'first_name'       => 'required', 
            'last_name'        => 'required', 
            'email'         => 'required|email|unique:users,email,'.$id, 
            'department_id'    => 'required',           
            'profile_image'    => 'image|max:2048',
            'password'          => 'nullable',
            'password_confirmation' => 'nullable|same:password'
        ]);     

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


        $user = User::findOrFail($id);
            $data = $request->all();
            if (isset($data['password']) && !empty($data['password'])) {
                $data['password'] = Hash::make($request->password);
            }else{
                unset($data['password']);
            }
            $data['department_id'] = implode(',',$data['department_id']);
            $profile_image =  $request->file('profile_image');
              if($profile_image){
                $imageName = time().'.'.$profile_image->extension();  
                $profile_image->storeAs('images', $imageName);
                $data['profile_image'] = $imageName;
                if ($user->profile_image!='' && \Storage::exists('images/'.$user->profile_image)) {
                    \Storage::delete('images/'.$user->profile_image);
                }
                $data['profile_image'] = $imageName;
              }else{
                $data['profile_image'] = 'https://via.placeholder.com/150';
                
              }
            $user->update($data);
            $request->session()->flash('success','User updated Successfully');
            return redirect()->to('admin/users');

        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{ 
            $user = User::findOrFail($id);
            $user->delete();
            session()->flash('danger','User deleted successfully');
            return redirect()->to('admin/users');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
        }

     
    public function getUsers(Request $request){
        $users = User::select([\DB::raw(with(new User)->getTable().'.*')]);
      return DataTables::of($users)
          ->editColumn('created_at', function($user){
              return date('Y-m-d', strtotime($user->created_at));
          })            
          ->addColumn('first_name', function($user){
              return $user->first_name;
          })
          ->addColumn('last_name', function($user){
            return $user->last_name;
        })
          ->addColumn('email', function($user){
              return $user->email;
          })
          ->editColumn('department_id', function ($user) {
            return $user->department_id;
          })
          ->addColumn('action', function ($user) {
              return
                  // edit
                  '<a href="'.route('users.edit',[$user->id]).'" class="btn btn-success btn-circle btn-sm"><i class="fas fa-edit"></i></a> '.
                  // Delete
                    Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit'=>"return confirm('Do you really want to delete?')",
                                'route' => ['users.destroy', $user->id])).
                    ' <button type="submit" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>'.
                    Form::close();
          })
          ->rawColumns(['action'])
          ->make(true);
  }
}
