@extends('admin.layouts.valex_app')
@section('styles')
@endsection
@section('content')
<div class="container">
    <div class="breadcrumb-header justify-content-between">
      <div class="left-content">
          <div>
            <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Department</h2>
          </div>
      </div>
    </div>
    <div class="row row-sm">
        <div class="col-xl-12">
            <div class="card">
            {!! Form::open(['method' => 'POST','url' => isset($department->id)?route('departments.update',[$department->id]):route('departments.store'),'class' => 'form-horizontal','id' => 'frmDepartment']) !!}
                @csrf
                @if(isset($department->id))
                @method('PUT')
                @endif
                <div class="card-header py-3 cstm_hdr">
                    <h6 class="m-0 font-weight-bold text-primary">Add Department</h6>
                </div>
                <div class="card-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label class="col-md-3 control-label" for="name">Name <span style="color:red">*</span></label>
                         <div class="col-md-6">
                        {!! Form::text('name', old('name', isset($department->name)? $department->name :''), ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                        @error('name')<small class="help-block">{{ $message }}</small> @enderror
                        </div>
                    </div>

                    <div class="form-group @error('description') has-error @enderror">
                        <label class="col-md-3 control-label" for="description">Description<span style="color:red">*</span></label>
                         <div class="col-md-6">
                            {!! Form::text('description', old('description', isset($department->description)? $department->description :''), ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            @error('description')<small class="help-block">{{ $message }}</small> @enderror
                        </div>
                    </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-responsive btn-primary">{{ __('Submit') }}</button>
                    <a href="{{url('admin/users')}}"  class="btn btn-responsive btn-secondary">{{ __('Cancel') }}</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>        
</div>
<!-- /.container-fluid -->
@endsection
@section('scripts')
@endsection