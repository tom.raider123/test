@extends('admin.layouts.valex_app')

@section('content')

<!-- Begin Page Content -->
<div class="container">
    <div class="breadcrumb-header justify-content-between">
					<div class="left-content">
						<div>
						  <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Hi, welcome back!</h2>
						  <p class="mg-b-0">Dashboard</p>
						</div>
					</div>
					<div class="main-dashboard-header-right" style="display: none;">
						<div>
							<label class="tx-13">Total User</label>
							<div class="main-star">
								<i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star"></i> <span>(14,873)</span>
							</div>
						</div>
					</div>
				</div>
				<!-- /breadcrumb -->
								<!-- row -->
				<div class="row row-sm">
					<div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
						<div class="card overflow-hidden sales-card bg-primary-gradient">
							<div class="pl-3 pt-3 pr-3 pb-2 pt-0">
								<div class="">
									<h6 class="mb-3 tx-12 text-white">Total Users</h6>
								</div>
								<div class="pb-0 mt-0">
									<div class="d-flex">
										<div class="">
											<h4 class="tx-20 font-weight-bold mb-1 text-white">{{  $users }}</h4>
											<p class="mb-0 tx-12 text-white op-7"></p>
										</div>
										<span class="float-right my-auto ml-auto">
											<!-- <i class="fas fa-arrow-circle-up text-white"></i> -->
											<span class="text-white op-7"></span>
										</span>
									</div>
								</div>
							</div>
							<span id="compositeline" class="pt-1">5,9,5,6,4,12,18,14,10,15,12,5,8,5,12,5,12,10,16,12</span>
						</div>
					</div>
				
					

				</div>
				<!-- row closed -->
				<!-- row opened -->
			
				<!-- row closed -->

				<!-- row opened -->
			
				<!-- row close -->

				<!-- row opened -->
			
		</div>
<!-- /.container-fluid -->
@endsection

@section('scripts')
<script src="{{ asset('template/valex-theme/js/index.js')}}"></script>
@endsection