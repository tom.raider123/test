@extends('admin.layouts.valex_app')
@section('styles')
<link href="{{asset('template/valex-theme/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="breadcrumb-header justify-content-between">
      <div class="left-content">
          <div>
            <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Users</h2>
          </div>
      </div>
    </div>
    <div class="row row-sm">
        <div class="col-xl-12">
            <div class="card">
            {!! Form::open(['method' => 'POST','files'=>true,'url' => isset($user->id)?route('users.update',[$user->id]):route('users.store'),'class' => 'form-horizontal','id' => 'frmUser']) !!}
                @csrf
                @if(isset($user->id))
                @method('PUT')
                @endif
                <div class="card-header py-3 cstm_hdr">
                    <h6 class="m-0 font-weight-bold text-primary">Add User</h6>
                </div>
                <div class="card-body">
                    <div class="form-group @error('first_name') has-error @enderror">
                        <label class="col-md-3 control-label" for="first_name">First name <span style="color:red">*</span></label>
                         <div class="col-md-6">
                        {!! Form::text('first_name', old('first_name', isset($user->first_name)? $user->first_name :''), ['class' => 'form-control', 'placeholder' => 'First name']) !!}
                        @error('first_name')<small class="help-block">{{ $message }}</small> @enderror
                        </div>
                    </div>

                    <div class="form-group @error('last_name') has-error @enderror">
                        <label class="col-md-3 control-label" for="last_name">Last name <span style="color:red">*</span></label>
                         <div class="col-md-6">
                            {!! Form::text('last_name', old('last_name', isset($user->last_name)? $user->last_name :''), ['class' => 'form-control', 'placeholder' => 'Last name']) !!}
                            @error('last_name')<small class="help-block">{{ $message }}</small> @enderror

                        </div>
                    </div>

                    <div class="form-group @error('email') has-error @enderror">
                        <label class="col-md-3 control-label" for="email">Email <span style="color:red">*</span></label>
                        <div class="col-md-6">
                            {!! Form::text('email',old('email', isset($user->email)? $user->email :''), ['class' => 'form-control autoFillOff', 'placeholder' => 'Email']) !!}
                            @error('email')<small class="help-block">{{ $message }}</small>@enderror
                        </div>
                    </div>
                    <div class="form-group @error('department_id') has-error  @enderror">
                        <label class="col-md-3 control-label" for="department_id">Department <span style="color:red">*</span></label>
                        <div class="col-md-6">
							<select class="select2 select2-multiple form-control" name="department_id[]" id="department_id" multiple="multiple">
								<option value="">-- Select Department --</option>
									@foreach ($department as $departments)
								<option value="{{ $departments->id }}" @if(isset($user)) @foreach(explode(',', $user->department_id) as $dep){{$dep == $departments->id ? 'selected': ''}}   @endforeach @endif>{{ $departments->name }}</option>
									@endforeach
							</select>
                
                            @error('department_id')<small class="help-block">{{ $message }}</small>@enderror
                        </div>
                    </div>
                    <div class="form-group @error('password') has-error  @enderror">
                        <label class="col-md-3 control-label" for="password">Password <span style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input type="password" name="password" class="form-control autoFillOff" placeholder="New Password" id="password">
                            @error('password')<small class="help-block">{{ $message }}</small>@enderror
                        </div>
                    </div>
                    <div class="form-group @error('password_confirmation') has-error  @enderror">
                        <label class="col-md-3 control-label" for="password_confirmation">Confirm Password <span style="color:red">*</span></label>
                        <div class="col-md-6">
                            <input type="password" name="password_confirmation" class="form-control autoFillOff" placeholder="Confirm Password" >
                            @error('password_confirmation')<small class="help-block">{{ $message }}</small>@enderror
                        </div>
                    </div>
                    <div class="form-group @error('profile_image') has-error  @enderror">                    
                        <label class="col-md-3 control-label" for="title">Profile image </label>
                        <div class="col-md-6">
                             <input type="file" name="profile_image" accept="image/*" id="profile_image">
                             <img src="{{ isset($user->prifile_image)? $user->profile_image : '' }}" width="50"> 
                            @error('profile_image')<small class="help-block">{{ $message }}</small>@enderror
                        </div>
                    </div>
                </div>  
                <div class="card-footer">
                    <button type="submit" class="btn btn-responsive btn-primary">{{ __('Submit') }}</button>
                    <a href="{{url('admin/users')}}"  class="btn btn-responsive btn-secondary">{{ __('Cancel') }}</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>        
</div>
<!-- /.container-fluid -->
@endsection
@section('scripts')
<script src="{{ asset('template/valex-theme/plugins/select2/js/select2.min.js') }}"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
    $('.select2').select2({
            placeholder: 'Choose',
            searchInputPlaceholder: 'Search'
    });
    
        
});
</script>
@endsection