<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DepartmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect('login');
});

Route::get('/logout', function(){
	Auth::logout();
    return redirect('/login');
});

Route::group(['middleware' => ['auth']], function() {
    //dashboard
    Route::get('admin/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    //user
    Route::resource('admin/users', UserController::class);
    Route::post('admin/users/getUsers', [UserController::class,'getUsers'])->name('admin.users.getUsers');
    //department
    Route::resource('admin/departments', DepartmentController::class);
    Route::post('admin/users/getDepartments', [DepartmentController::class,'getDepartments'])->name('admin.users.getDepartments');
});
Auth::routes();